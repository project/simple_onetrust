(function ($, Drupal, drupalSettings) {
  let scriptClass =
    $("script[src^='//amp.azure.net']").length &&
    $("script[src^='//amp.azure.net']").attr('class').split(' ')[0].split('-');
  let azuremeGroupId = scriptClass[scriptClass.length - 1];

  // Add block message
  Drupal.behaviors.onetrust_block_message = {
    attach: function (context, settings) {
      let reg = new RegExp('!simple_onetrust_from_src_url', 'g');
      let iframes = $("iframe[class^='optanon-category']");

      if (typeof OptanonActiveGroups === 'undefined') {
        return false;
      }

      // iframe video
      if (iframes.length) {
        for (let i = 0; i < iframes.length; i++) {
          let oIframe = iframes.eq(i);
          let src = oIframe.attr('data-src');
          let oIframeClass = oIframe.attr('class').split(' ')[0].split('-');
          let groupId = oIframeClass[oIframeClass.length - 1];

          if (src === undefined || oIframe.next('.onetrust-message-wrapper').length) {
            continue;
          }

          if (oIframe.hasClass('optanon-category-C0003') && !OptanonActiveGroups.includes(groupId)) {
            let oEle = `<div class='onetrust-message-wrapper'>${settings.simple_onetrust.block_notice_c003.replace(
              reg,
              src
            )}</div>`;
            oIframe.after($(oEle));
            oIframe.next('.onetrust-message-wrapper').on('click', function () {
              $(`#ot-header-id-${groupId}`).click();
            })
            if (Drupal.behaviors.extlink !== undefined) {
              Drupal.behaviors.extlink.attach(document, drupalSettings);
            }
          } else if (oIframe.hasClass('optanon-category-C0004') && !OptanonActiveGroups.includes(groupId)) {
            let oEle = `<div class='onetrust-message-wrapper'>${settings.simple_onetrust.block_notice_c004.replace(
              reg,
              src
            )}</div>`;
            oIframe.after($(oEle));
            oIframe.next('.onetrust-message-wrapper').on('click', function () {
              $(`#ot-header-id-${groupId}`).click();
            })
            if (Drupal.behaviors.extlink !== undefined) {
              Drupal.behaviors.extlink.attach(document, drupalSettings);
            }
          }
        }
      }

      // Azuremediaplayer video
      if (
        scriptClass &&
        !OptanonActiveGroups.includes(azuremeGroupId)
      ) {
        for (let i = 0, len = $('.azuremediaplayer').length; i < len; i++) {
          let oSource = $('.azuremediaplayer').eq(i);
          let src = oSource.children('source').attr('src');

          if (
            src === undefined ||
            oSource.next('.onetrust-message-wrapper').length
          ) {
            continue;
          }

          let oEle = `<div class='onetrust-message-wrapper'>${settings.simple_onetrust.block_notice_c004.replace(
            reg,
            src
          )}</div>`;
          oSource.hide().after($(oEle));
          oSource.next('.onetrust-message-wrapper').on('click', function () {
            $(`#ot-header-id-${azuremeGroupId}`).click();
          });
        }
      }
    },
  };

  // Block message for fancybox
  const observer = new MutationObserver(function (mutationsList) {
    for (let mutation of mutationsList) {
      if (
        $(mutation.target).hasClass('fancybox-iframe') &&
        !$(mutation.target).next().length
      ) {
        Drupal.behaviors.onetrust_block_message.attach(document, drupalSettings);
      }
    }
  });

  observer.observe(document.documentElement, {
    childList: !0,
    subtree: !0,
    attributes: !0,
    attributeFilter: ['src'],
  });

  // Remove block message
  let clearTime = 0;
  const Timer = setInterval(function () {
    if (typeof OneTrust !== 'undefined') {
      Drupal.behaviors.onetrust_block_message.attach(document, drupalSettings);

      OneTrust.OnConsentChanged(function () {
        // Remove block message
        let iframes = $("iframe[class^='optanon-category']");

        // Remove iframe block message
        if (iframes.length) {
          for (var i = 0; i < iframes.length; i++) {
            let oIframe = iframes.eq(i);
            let src = oIframe.attr('src');
            let oIframeClass = oIframe.attr('class').split(' ')[0].split('-');
            let groupId = oIframeClass[oIframeClass.length - 1];

            if (src === undefined) {
              continue;
            }

            if (OptanonActiveGroups.includes(groupId)) {
              oIframe.next('.onetrust-message-wrapper').remove();
              // Special remove for iframe-container
              oIframe.parent().next('.onetrust-message-wrapper').remove();
            }
          }
        }

        // Remove azuremediaplayer block message
        if (scriptClass && OptanonActiveGroups.includes(azuremeGroupId)) {
          for (let i = 0, len = $('.azuremediaplayer').length; i < len; i++) {
            let oSource = $('.azuremediaplayer').eq(i).children('source');
            let src = oSource.attr('src');

            if (src === undefined) {
              continue;
            }

            $('.azuremediaplayer')
              .eq(i)
              .show()
              .next('.onetrust-message-wrapper')
              .remove();
          }
        }
      });
      clearInterval(Timer);
    } else if (clearTime === 50) {
      clearInterval(Timer);
    }
    clearTime += 1;
  }, 200);
})(jQuery, Drupal, drupalSettings);
